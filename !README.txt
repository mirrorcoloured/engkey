##########
# EngKey #
##########

Version: 15
Compiled: 171228
By: Sky Chrastina

###############
# GENERAL USE #
###############

Run EngKey_15.exe

The golden gear icon in the taskbar shows that the program is running. You can right click it to pause or close it.

All typing shortcuts begin with ;

Some examples are:

;D		Δ
;S		Σ
;z		ζ
;methanol	CH₃OH
;^2		²
;_a		ₐ
;forall	∀
;v_		v̅
;<=		≤
;=...	∴
;.		·
;star		★
;coffee		☕

Greek letters are simply the english letter equivilent
Diacritics are the letter followed by the closest symbol (\ for grave, : for umlaut, etc)
Symbols are either combinations of other symbols, or a word describing them
Superscripts all begin with ^
Subscripts all begin with _

See the excel sheet for a full list of shortcuts.

Send any suggestions to pjschrastina@gmail.com

Enjoy!

############
# CONTENTS #
############

!README.txt
	This very file! Tells you what's up.

EngKey_15.ahk
	Uncompiled file. Open in notepad to see AHK code.

EngKey_15.exe
	Compiled file. Run this one for great victory!

EngKey_15.xlsm
	Details file. Use this as a reference to see what shortcuts are available in this version.
