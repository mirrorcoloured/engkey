; #################### INFO ####################
; EngKey_DEV
; Sky Chrastina
; 180212

; HOTKEYS:
;	^		Control
;	+		Shift
;	!		Alt
;	#		WindowsKey
;	~		Don't suppress triggering key
;	$		Disable triggers from within other hotkeys
;	{F1}
;	{Shift Down}

; https://en.wikipedia.org/wiki/List_of_Unicode_characters

; :*:;_::_

; #################### WINDOWGROUP SETUP ####################

Menu, Tray, Icon, gear_yellow.ico, , 1

GroupAdd, latexgroup, ahk_exe miktex-texworks.exe
GroupAdd, latexgroup, Insert Equation - Zim
GroupAdd, latexgroup, Edit Equation - Zim
GroupAdd, pythongroup, ahk_exe Code.exe
GroupAdd, pythongroup, ahk_exe pythonw.exe
GroupAdd, pythongroup, ahk_exe mintty.exe
GroupAdd, jsgroup, ahk_exe Brackets.exe 
GroupAdd, jsgroup, ahk_exe Code.exe

; #################### OPTIONS ####################

#Hotstring EndChars ()[]{};'"`n `t
#Hotstring c o ?
	; c = case sensitive
	; o = omit ending character
	; ? = trigger inside words

:*:;update::
	Run https://gitlab.com/mirrorcoloured/engkey/raw/master/EngKey.exe
	Return

; Launch shell (winkey + tilde)
#`::
	Run D:/programs/Git/git-bash.exe --cd="D:/projects"
	Return

; Get current window details (ctrl+shift+space)
^+Space::
	PID = 0
	WinGet, hWnd,, A
	DllCall("GetWindowThreadProcessId", "UInt", hWnd, "UInt *", PID)
	hProcess := DllCall("OpenProcess",  "UInt", 0x400 | 0x10, "Int", False,  "UInt", PID)
	PathLength = 260*2
	VarSetCapacity(FilePath, PathLength, 0)
	DllCall("Psapi.dll\GetModuleFileNameExW", "UInt", hProcess, "Int", 0, "Str", FilePath, "UInt", PathLength)
	DllCall("CloseHandle", "UInt", hProcess)
	MsgBox, %FilePath%
	Return

; Run browser snippet
f8::
	Send, ^+j
	Sleep, 200
	Send, ^o
	Sleep, 200
	Send, {!}ColorCorityFields.js
	Sleep, 200
	Send, {Enter}{F12}
	Return

; Replace backslashes with forward slashes
!/::
	; OldClipboard := ClipboardAll
	Clipboard := ""
	SendInput, ^c
	ClipWait
	StringReplace, Clipboard, Clipboard, \, /, All)
	SendInput, ^v
	; Clipboard := OldClipboard
	Return

;F10::Send,""{Left}{Enter}
;F11::Send,^{Backspace}{Backspace}{Delete}{Enter}{End}

$Media_Play_Pause::
	;WinActivate, ahk_exe firefox.exe
	;WinActivate, ahk_exe PlexMediaPlayer.exe
	Send {Media_Play_Pause}
	;Send !{Tab}
	Return

; Generate UUID
:*:;uuid::
	u := ComObjCreate("Scriptlet.TypeLib").GUID
	u := SubStr(u, 2, -3)
	Send, %u%
	Return

RandomFromFile(filename) {
	FileRead, Contents, %filename%
	Sort, Contents, Random
	Loop, parse, Contents, `n, `r
	{
		Return %A_Loopfield%
		break
	}
	Return
}

:*:;noun::
	n := RandomFromFile("D:/datasets/nouns.txt")
	Send %n%
	Return

:*:;verb::
	v := RandomFromFile("D:/datasets/verbs.txt")
	Send %v%
	Return

:*:;adjective::
	a := RandomFromFile("D:/datasets/adjectives.txt")
	Send %a%
	Return

:*:;van::
	n := RandomFromFile("D:/datasets/nouns.txt")
	v := RandomFromFile("D:/datasets/verbs.txt")
	a := RandomFromFile("D:/datasets/adjectives.txt")
	Send %v% %a% %n%
	Return

; #################### TEMPORARY ARCADIS SPECIFIC ####################

:*:;ac20::Arcadis2020{!}
:*:;acmy::TestmyCorityUser


; #################### NOTEPAD SPECIFIC ####################

#IfWinActive ahk_exe notepad.exe

; Refresh script upon saving
~^s::Reload

; #################### EXCEL SPECIFIC ####################

#IfWinActive ahk_exe EXCEL.exe

; Disable F1
F1::Return

; #################### JS SPECIFIC ####################

#IfWinActive ahk_group jsgroup

:*:;log::{ESC}console.log();{Left 2}
:*:;css::<link type="text/css" rel="stylesheet" href=".css">{LEFT 6}
:*:;js::<script type="module" src=".js"></script>{LEFT 14}
:*:;//::/**{Enter}{Down}{Home}{Tab}{Up}{Home}{Tab}{End}
:*:;prop::/**{Enter}{Tab}@name {Enter}@type {Enter}@desc {Enter}{Backspace}*/{Up 3}{End}
:*:;pf::@param {{}float{}}
:*:;pi::@param {{}int{}}
:*:;ps::@param {{}string{}}
:*:;pb::@param {{}boolean{}}
:*:;link::{{}@link {}}{Left}
:*:;iife::(function () {{}return "";{}})()

; #################### PYTHON SPECIFIC #################### 

#IfWinActive ahk_group pythongroup

; Wrap current line in print()
:*:;pp::
	SendInput {End}){Home}print(
	Return
:*:;#79::{# 79}
:*:;class::class foo():{Return}def __init__(self):{Return}
:*:;"::"""{Return 2}"""{Up}
:*:;decorator::def decor(f):{Return}from functools import wraps{Return}@wraps(f){Return}def wrapf(*args, **kwargs):{Return}{Tab}pass{Return}f(*args, **kwargs){Return}{Backspace}return wrapf{Up 2}{Shift Down}{Home}{Shift Up}
:*:;main::if __name__ == "__main__":{Return}

; #################### MATHEMATICA SPECIFIC ####################

#IfWinActive ahk_exe Mathematica.exe

:*:;clear::ClearAll[Evaluate[$Context<>"*"]];
:*:;simple::Simple[x_]:=Simplify[x,t\[Element]Reals];
:*:;label::Dis[var_,post_:""]:=Row[{{}var," = ",ToExpression[var]," ",post{}}];
:*:;solve::Set @@@ Quiet[Solve[{{}{}},{{}{}}]][[1]]{Left 11}
:*:;setsolve::SetSolve[exp_, var_]:=Row[{{}var, " = ", Set @@@ Solve[exp, var][[1]]{}}]
:*://::(**){Left 2}{Space 2}{Left}
:*:;ee::*10^6
:*:;d::{Esc}delta{Esc}
:*:;D::{Esc}Delta{Esc}
::;e::{Esc}epsilon{Esc}
:*:;p::{Esc}pi{Esc}
:*:;r::{Esc}rho{Esc}
::;s::{Esc}sigma{Esc}
:*:;u::{Esc}theta{Esc}

; ################## LATEX SPECIFIC #####################

#IfWinActive ahk_group latexgroup

:*:;emph::\emph{{}{}}{Left}
:*:;tt::\texttt{{}{}}{Left}
:*:;code::\begin{{}lstlisting{}}{Return 2}\end{{}lstlisting{}}{Up}+{Home}{Delete}
:*:;verbatim::\begin{{}verbatim{}}{Return 2}\end{{}verbatim{}}{Up}{Tab}
:*:;enumerate::\begin{{}enumerate{}}{Return 2}\end{{}enumerate{}}{Up}{Tab}\item 
:*:;align::\begin{{}align*{}}{Return 2}\end{{}align*{}}{Up}{Tab}
:*:;image::\begin{{}center{}}{Return 2}\end{{}center{}}{Up}{Tab}\includegraphics[scale=1]{{}img.png{}}
:*:;picture::\begin{{}center{}}{Return 2}\end{{}center{}}{Up}{Tab}\includegraphics[scale=1]{{}img.png{}}
:*:;graphic::\begin{{}center{}}{Return 2}\end{{}center{}}{Up}{Tab}\includegraphics[scale=1]{{}img.png{}}
:*:;tf::\tfrac{{}{}}{{}{}}{Left 3}
:*:;_0::$_0$
:*:;_1::$_1$
:*:;_2::$_2$
:*:;_3::$_3$
:*:;_4::$_4$
:*:;_5::$_5$
:*:;_6::$_6$
:*:;_7::$_7$
:*:;_8::$_8$
:*:;_9::$_9$
:*:;frac::\frac{{}{}}{{}{}}{Left 3}
:*:;int::\int_{{}{}}{^}{{}{}}{Left 4}
:*:;eval::\biggr{{}|{}}_{{}{}}{^}{{}{}}{Left 4}
:*:;bar::\overline{{}{}}{Left}
:*:;dot::\dot{{}{}}{Left}
::;d::\delta
:*:;D::\Delta
::;e::\epsilon
:*:;s::\sigma
::;t::\tau
; Kinetics
:*:;ca0::c_{{}A_0{}}
::;ca::c_A
:*:;xa::x_A
:*:;ea::\epsilon_A
:*:;.::{Space}\cdot{Space}
:*:;v.0::\dot{{}V{}}_0
:*:;ca0::c_{{}A_0{}}

; #################### ZIM SPECIFIC ####################

#IfWinActive ahk_exe zim.exe

::;today::
	FormatTime, CurrentDate,, yyyy:MM:dd
	SendInput [[Journal:%CurrentDate%]]
	Return
::;now::
	FormatTime, CurrentDate,, yyyy:MM:dd
	FormatTime, CurrentTime,, HHmm
	SendInput [[Journal:%CurrentDate%]] %CurrentTime%
	Return

:*:;unittemp::
	SendInput ^bRepresentations{:}{Enter 3}^bUnits{:}{Enter 3}^bDerived from{:}{Enter 3}^bPart of{:}{Enter 3}^bUsed in{:}{Enter 3}^bExamples{:}{Enter}
	Return

; Copy line above
:*:;copy::{Up}{Home}{Shift Down}{End}{Shift Up}^c{Down}^v

; Insert LaTeX equation
:*:;eqn::!i{Up 7}{Enter}

:*:;code::{{}{{}{{}code:{Enter 2}{}}{}}{}}{Up}

; #################### GENERAL ####################

#IfWinActive

:*:;date::
	FormatTime, CurrentDateTime,, yyyy.MM.dd
	SendInput %CurrentDateTime%
	Return
:*:;time::
	FormatTime, CurrentDateTime,, HH:mm:ss
	SendInput %CurrentDateTime%
	Return
:*:;now::
	FormatTime, CurrentDateTime,, yy.MM.dd-HH:mm:ss
	SendInput %CurrentDateTime%
	Return

~F12::
	Send {CtrlBreak}
	Send {Pause}
	Send ^{Pause}
	Send ^{Break}
	Return

; Shortcuts
:*:;{}::{{}{{}{}}{{}{}}{}}{Left}{Left}{Left}

; Greek
::;A::Α
::;B::Β
::;C::Ψ
::;D::Δ
::;E::Ε
::;F::Φ
::;G::Γ
::;H::Η
::;I::Ι
::;J::Ξ
::;K::Κ
::;L::Λ
::;M::Μ
::;N::Ν
::;O::Ο
::;P::Π
::;Q::;
::;R::Ρ
::;S::Σ
::;T::Τ
::;U::Θ
::;V::Ω
::;W::ς
::;X::Χ
::;Y::Υ
::;Z::Ζ
::;a::α
::;b::β
::;c::ψ
::;d::δ
::;e::ε
::;f::φ
::;g::γ
::;ga::Ɣ
::;h::η
::;i::ι
::;j::ξ
::;k::κ
::;l::λ
::;m::μ
::;n::ν
::;o::ο
::;p::π
::;q::;
::;r::ρ
::;s::σ
::;t::τ
::;u::θ
::;v::ω
::;w::ς
::;x::χ
::;y::υ
::;z::ζ

; Cedilla
:*:;C,::Ç
:*:;c,::ç

; Dot 0307
:*:;F.::Ḟ
:*:;K.::K̇
:*:;k.::k̇
:*:;M.::Ṁ
:*:;m.::ṁ
:*:;N.::Ṅ
:*:;n.::ṅ
:*:;O.::Ȯ
:*:;o.::ȯ
:*:;P.::Ṗ
:*:;p.::ṗ
:*:;Q.::Q̇
:*:;q.::q̇
:*:;R.::Ṙ
:*:;r.::ṙ
:*:;S.::Ṡ
:*:;s.::ṡ
:*:;T.::Ṫ
:*:;t.::ṫ
:*:;U.::U̇
:*:;u.::u̇
:*:;V.::V̇
::;v.::v̇
:*:;W.::Ẇ
:*:;w.::ẇ
:*:;X.::Ẋ
:*:;x.::ẋ
:*:;Y.::Ẏ
:*:;y.::ẏ
:*:;Z.::Ż
:*:;z.::ż

; Diaeresis / Umlaut 0308
:*:;e: ::ë
:*:;K: ::K̈
:*:;k: ::k̈
:*:;M: ::M̈
:*:;m: ::m̈
:*:;N: ::N̈
:*:;n: ::n̈
:*:;O: ::Ö
:*:;o: ::ö
:*:;P: ::P̈
:*:;p: ::p̈
:*:;Q: ::Q̈
:*:;q: ::q̈
:*:;R: ::R̈
:*:;r: ::r̈
:*:;S: ::S̈
:*:;s: ::s̈
:*:;U: ::Ü
:*:;u: ::ü
:*:;V: ::V̈
:*:;v: ::v̈
:*:;W: ::Ẅ
:*:;w: ::ẅ
:*:;X: ::Ẍ
:*:;x: ::ẍ
:*:;Y: ::Ÿ
:*:;y: ::ÿ
:*:;Z: ::Z̈
:*:;z: ::z̈

; Acute
:*:;Α/::Á
:*:;a/::á
:*:;C/::Ć
:*:;c/::ć
:*:;E/::É
:*:;e/::é
:*:;G/::Ǵ
:*:;g/::ǵ
:*:;I/::Í
:*:;i/::í
:*:;K/::Ḱ
:*:;k/::ḱ
:*:;L/::Ĺ
:*:;l/::ĺ
:*:;M/::Ḿ
:*:;m/::ḿ
:*:;N/::Ń
:*:;n/::ń
:*:;O/::Ó
:*:;o/::ó
:*:;P/::Ṕ
:*:;p/::ṕ
:*:;R/::Ŕ
:*:;r/::ŕ
:*:;S/::Ś
:*:;s/::ś
:*:;U/::Ú
:*:;u/::ú
:*:;W/::Ẃ
:*:;w/::ẃ
:*:;X/::X́
:*:;x/::x́
:*:;Y/::Ý
:*:;y/::ý
:*:;Z/::Ź
:*:;z/::ź

; Grave
:*:;A\::À
:*:;a\::à
:*:;E\::È
:*:;e\::è
:*:;I\::Ì
:*:;i\::ì
:*:;N\::Ǹ
:*:;n\::ǹ
:*:;O\::Ò
:*:;o\::ò
:*:;U\::Ù
:*:;u\::ù
:*:;W\::Ẁ
:*:;w\::ẁ
:*:;Y\::Ỳ
:*:;y\::ỳ

; Circumflex 0302 0359 0360
:*:;A^::Â
:*:;a^::â
:*:;C^::Ĉ
:*:;c^::ĉ
:*:;E^::Ê
:*:;e^::ê
:*:;G^::Ĝ
:*:;g^::ĝ
:*:;H^::Ĥ
:*:;h^::ĥ
:*:;I^::Î
:*:;i^::î
:*:;J^::Ĵ
:*:;j^::ĵ
:*:;k^::k̂
:*:;n^::n̂
:*:;O^::Ô
:*:;o^::ô
:*:;S^::Ŝ
:*:;s^::ŝ
:*:;U^::Û
:*:;u^::û
:*:;V^::V̂
:*:;v^::v̂
:*:;W^::Ŵ
:*:;w^::ŵ
:*:;Y^::Ŷ
:*:;y^::ŷ
:*:;Z^::Ẑ
:*:;z^::ẑ

; Squiggle
:*:;n~::ñ

; Dash through
:*:;D-::Ð

; Overbar 0773 before character
:*:;0_::̅0
:*:;1_::̅1
:*:;2_::̅2
:*:;3_::̅3
:*:;4_::̅4
:*:;5_::̅5
:*:;6_::̅6
:*:;7_::̅7
:*:;8_::̅8
:*:;9_::̅9
:*:;a_::a̅
:*:;b_::b̅
:*:;c_::c̅
:*:;d_::d̅
:*:;e_::e̅
:*:;f_::f̅
:*:;G_::G̅
:*:;g_::g̅
:*:;h_::h̅
:*:;i_::i̅
:*:;j_::j̅
:*:;k_::k̅
:*:;L_::L̅
:*:;l_::l̅
:*:;M_::̅M
:*:;m_::m̅
:*:;N_::N̅
:*:;n_::n̅
:*:;o_::o̅
:*:;P_::̅P
:*:;p_::p̅
:*:;q_::q̅
:*:;R_::R̅
:*:;r_::r̅
:*:;s_::s̅
:*:;t_::t̅
:*:;u_::u̅
:*:;V_::V̅
:*:;v_::v̅
:*:;w_::w̅
:*:;x_::x̅
:*:;y_::y̅
:*:;z_::z̅

; Flipped characters
:*:;flipa::ɐ
:*:;flipc::ɔ
:*:;flipe::ǝ
:*:;flipf::ɟ
:*:;flipg::ƃ
:*:;fliph::ɥ
:*:;flipi::ᴉ
:*:;flipk::ʞ
:*:;flipm::ɯ
:*:;flipr::ɹ
::;flipt::ʇ
:*:;flipv::ʌ
:*:;flipw::ʍ
:*:;flipy::ʎ
:*:;flipA::∀
:*:;flipC::Ɔ
:*:;flipE::Ǝ
:*:;flipF::Ⅎ
:*:;flipG::פ
:*:;flipJ::ſ
:*:;flipP::Ԁ
:*:;flipT::┴
:*:;flipV::Λ
:*:;flipY::⅄
:*:;flip.::˙
:*:;flip?::¿
:*:;flip!::¡
:*:;flip&::⅋
:*:;flip_::‾

; Symbol
:*:;deg::°
:*:;der::Dₓ
::;int::∫
::;1int::∮
::;2int::∯
::;3int::∰
:*:;inf::∞
:*:;infinity::∞
:*:;laplace::ℒ
:*:;e-::e⁻
:*:;star::★
:*:;spade::♠
:*:;club::♣
:*:;heart::♥
:*:;diamond::♦
:*:;check::✓
::;ex::✗
:*:;-=::≠
:*:;~=::≈
:*:;==::≡
:*:;<=::≤
:*:;>=::≥
:*:;<<::«
:*:;>>::»
:*:;+-::±
:*:;-+::∓
:*:;*::×
:*:;.::·
:*:;blank::⠀
:*:;bullet::•
:*:;dot::●
:*:;dash::▬
::;/::÷
:*:;angle::∠
:*:;mangle::∡
:*:;sangle::∢
:*:;v...::⋮
:*:;h...::⋯
:*:;u...::⋰
:*:;d...::⋱
:*:;=...::∴
:*:;because::∵
:*:;wave::〰
:*:;exists::∃
:*:;nexists::∄
:*:;proportional::∝
:*:;0::∅
:*:;and::∧
:*:;or::∨
:*:;nor::⊽
:*:;xor::⊕
:*:;x or::⊻
:*:;nand::⊼
:*:;not::¬
:*:;rnot::⌐
:*:;union::∪
:*:;intersect::∩
:*:;perp::⊥
:*:;para::∥
:*:;elementof::∈
:*:;subsetof::⊆
:*:;forall::∀
:*:;root::√
:*:;sqrt::√
:*:;del::∇
:*:;hourglass::⧖
::;RR::ℝ
:*:;RR2::ℝ²
:*:;RR3::ℝ³
::;PP::ℙ
:*:;PP1::ℙ₁
:*:;PP2::ℙ₂
:*:;PP3::ℙ₃
::;NN::ℕ
::;ZZ::ℤ
::;CC::ℂ
::;QQ::ℚ
::;DD::ⅅ
:*:;EE::ᴇ
::;ee::ⅇ
::;dd::ⅆ
::;ii::ⅈ
::;jj::ⅉ
::;imag::ℑ
::;real::ℜ
:*:;tup::▲
:*:;tdown::▼
:*:;tleft::◄
:*:;tright::►
::;up::↑
::;down::↓
::;left::←
::;right::→
:*:;lr::↔
:*:;ud::↕
:*:;dr::↳
:*:;rright::⇒
:*:;equi::⇌
:*:;res::↔
:*:;invis::​
::;-::–{Space}
:*:;--::—
:*:;Ao::Å
:*:;ao::å
:*:;AE::Æ
:*:;ae::æ
:*:;e0::ε₀
:*:;m0::μ₀
:*:;u0::μ₀
:*:;o0::ω₀
:*:;w0::ω₀
:*:;ff::ƒ
:*:;ll::ℓ
:*:;O/::Ø
:*:;o/::ø
:*:;c|::¢
:*:;L-::£
:*:;ox::¤
:*:;Y=::¥
:*:;F=::₣
:*:;L=::₤
:*:;pj::₪
:*:;E=::€
:*:;SS::§
:*:;(C)::©
:*:;(R)::®
:*:;TM::™
:*:;|-::†
:*:;|=::‡
:*:;paragraph::¶
:*:;block::█
:*:;:)::☺
:*:;:(::☹
:*:;eyes::👀
:*:;ankh::☥
:*:;biohazard::☣
:*:;christianity::✝
:*:;communism::☭
:*:;islam::☪
:*:;judaism::✡
:*:;radiation::☢
:*:;jollyroger::☠
:*:;yinyang::☯
:*:;smile::◕‿◕
:*:;shrug::¯\_(ツ)_/¯
:*:;fliptable::(╯°□°）╯︵ ┻━┻
:*:;tableflip::┬─┬﻿ ︵ /(.□. \）
:*:;unfliptable::┬─┬﻿ ノ( ゜-゜ノ)
:*:;cheers::（ {^}_{^}）o自自o（{^}_{^} ）
:*:;tripping::q(❂‿❂)p
:*:;ayyy::(☞ﾟヮﾟ)☞
:*:;wired::⊙﹏⊙
:*:;sunglasses::•_•){Return}( •_•)>⌐■-■{Return}(⌐■_■)
:*:;wow::⊙▂⊙
:*:;muchwow::⊙▃⊙
:*:;themoreyouknow::≈≈≈≈≈★
:*:;WHY::щ(°Д°щ)
:*:;loud::°Д°
:*:;angry::щ(ಠ益ಠщ)
:*:;thumbsup::👍
:*:;peace::✌
:*:;ok::👌
:*:;horns::🤘
:*:;peacesign::☮
:*:;umbrella::☂
:*:;coffee::☕
::;sun::☼{Space}
:*:;female::♀
:*:;male::♂
:*:;music::♪
:*:;dmusic::♫
:*:;1/2::½
:*:;1/3::⅓
:*:;2/3::⅔
:*:;1/4::¼
:*:;3/4::¾
:*:;1/8::⅛
:*:;3/8::⅜
:*:;5/8::⅝
:*:;7/8::⅞
::;?::¿{Space}
::;!::¡{Space}
:*:?!::‽
:*:!?::‽

; Compounds
:*:;h2o::H₂O
:*:;h3o::H₃O⁺
:*:;oh::OH⁻
:*:;methane::CH₄
:*:;ethane::C₂H₆
:*:;propane::C₃H₈
:*:;butane::C₄H₁₀
:*:;pentane::C₅H₁₂
:*:;hexane::C₆H₁₄
:*:;octane::C₈H₁₈
:*:;ethene::C₂H₄
:*:;propene::C₃H₆
:*:;butene::C₄H₈
:*:;pentene::C₅H₁₀
:*:;hexene::C₆H₁₂
:*:;octene::C₈H₁₆
:*:;methanol::CH₃OH
:*:;ethanol::C₂H₅OH
:*:;propanol::C₃H₇OH
:*:;benzene::C₆H₆
:*:;cyclohexane::C₆H₁₂
:*:;acetone::(CH₃)₂CO
:*:;acetylene::C₂H₂
:*:;ammonia::NH₃
:*:;ammonium::NH₄OH
:*:;ether::(C₂H₅)₂O
:*:;formaldehyde::CH₂O
:*:;nitric::HNO₃
:*:;sulfuric::H₂SO₄
:*:;phosphoric::H₃PO₄
:*:;potash::K₂CO₃
:*:;chalk::CaCO₃
:*:;sodaash::Na₂CO₃
:*:;urea::CO(NH₂)₂
:*:;glycerine::C₃H₅(OH)₃
:*:;caffeine::C₈H₁₀N₄O₂
:*:;aspirin::C₉H₈O₄
:*:;menthol::C₁₀H₂₀O
:*:;glucose::C₆H₁₂O₆
:*:;sucrose::C₁₂H₂₂O₁₁
:*:;o2::O₂
:*:;n2::N₂
::;h2::H₂{Space}
:*:;co2::CO₂
:*:;so2::SO₂
:*:;h2s::H₂S
:*:;nano3::NaNO₃
:*:;cacl2::CaCl₂
:*:;mgso4::MgSO₄
:*:;k2hpo4::K₂HPO₄
:*:;kh2po4::KH₂PO₄
:*:;nacl::NaCl
:*:;fecl3::FeCl₃
:*:;mncl2::MnCl₂
:*:;znso4::ZnSO₄
:*:;cocl2::CoCl₂
:*:;na2moo4::Na₂MoO₄
:*:;cuso4::CuSO₄
:*:;h3bo3::H₃BO₃
:*:;.2h::∙2H₂O
:*:;.3h::∙3H₂O
:*:;.4h::∙4H₂O
:*:;.5h::∙5H₂O
:*:;.6h::∙6H₂O
:*:;.7h::∙7H₂O

; Elements
:*:;pt01::hydrogen
:*:;pt02::helium
:*:;pt03::lithium
:*:;pt04::beryllium
:*:;pt05::boron
:*:;pt06::carbon
:*:;pt07::nitrogen
:*:;pt08::oxygen
:*:;pt09::fluorine
:*:;pt10::neon
:*:;pt11::sodium
:*:;pt12::magnesium
:*:;pt13::aluminum
:*:;pt14::silicon
:*:;pt15::phosphorus
:*:;pt16::sulfur
:*:;pt17::chlorine
:*:;pt18::argon
:*:;pt19::potassium
:*:;pt20::calcium
:*:;pt21::scandium
:*:;pt22::titanium
:*:;pt23::vanadium
:*:;pt24::chromium
:*:;pt25::manganese
:*:;pt26::iron
:*:;pt27::cobalt
:*:;pt28::nickel
:*:;pt29::copper
:*:;pt30::zinc

; Sets
:*:;doublestruck::𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫𝟘𝟙𝟚𝟛𝟜𝟝𝟞𝟟𝟠𝟡
:*:;mathscript::𝒜ℬ𝒞𝒟ℰℱ𝒢ℋℐ𝒥𝒦ℒℳ𝒩𝒪𝒫𝒬ℛ𝒮𝒯𝒰𝒱𝒲𝒳𝒴𝒵𝒶𝒷𝒸𝒹ℯ𝒻ℊ𝒽𝒾𝒿𝓀𝓁𝓂𝓃ℴ𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏
:*:;fraktur::𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷
:*:;superscript::⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻⁼⁽⁾ⁿⁱ
:*:;subscript::₀₁₂₃₄₅₆₇₈₉₊₋₌₍₎ₐₑₕᵢⱼₖₗₘₙₒₚᵣₛₜᵤᵥₓₔ
:*:;greekupper::ΑΒΨΔΕΦΓΗΙΞΚΛΜΝΟΠ:ΡΣΤΘΩ΅ΧΥΖ
:*:;greeklower::αβψδεφγηιξκλμνοπ;ρστθωςχυζ
:*:;currency::¤₳฿₵​¢​₡​₢$​₫​₯​֏₠€ƒ₣₲₴₭₺₾ℳ₥₦₧₱₰£៛₽₹₨₪৳​₸₮₩¥

; Macros
:*:;reac::—()→{Left 2}

; Superscript
:*:;^0::⁰
:*:;^1::¹
:*:;^2::²
:*:;^3::³
:*:;^4::⁴
:*:;^5::⁵
:*:;^6::⁶
:*:;^7::⁷
:*:;^8::⁸
:*:;^9::⁹
:*:;^+::⁺
:*:;^-::⁻
:*:;^=::⁼
:*:;^(::⁽
:*:;^)::⁾
:*:;^a::ᵃ
:*:;^b::ᵇ
:*:;^c::ᶜ
:*:;^d::ᵈ
:*:;^e::ᵉ
:*:;^f::ᶠ
:*:;^g::ᵍ
:*:;^h::ʰ
:*:;^i::ⁱ
:*:;^j::ʲ
:*:;^k::ᵏ
:*:;^l::ˡ
:*:;^m::ᵐ
:*:;^n::ⁿ
:*:;^o::ᵒ
:*:;^p::ᵖ
:*:;^r::ʳ
:*:;^s::ˢ
:*:;^t::ᵗ
:*:;^u::ᵘ
:*:;^v::ᵛ
:*:;^w::ʷ
:*:;^x::ˣ
:*:;^y::ʸ
:*:;^z::ᶻ
:*:;^A::ᴬ
:*:;^B::ᴮ
:*:;^D::ᴰ
:*:;^E::ᴱ
:*:;^G::ᴳ
:*:;^H::ᴴ
:*:;^I::ᴵ
:*:;^J::ᴶ
:*:;^K::ᴷ
:*:;^L::ᴸ
:*:;^M::ᴹ
:*:;^N::ᴺ
:*:;^O::ᴼ
:*:;^P::ᴾ
:*:;^R::ᴿ
:*:;^T::ᵀ
:*:;^U::ᵁ
:*:;^V::ⱽ
:*:;^W::ᵂ

; Subscript
:*:;_0::₀
:*:;_1::₁
:*:;_2::₂
:*:;_3::₃
:*:;_4::₄
:*:;_5::₅
:*:;_6::₆
:*:;_7::₇
:*:;_8::₈
:*:;_9::₉
:*:;_+::₊
:*:;_-::₋
:*:;_=::₌
:*:;_(::₍
:*:;_)::₎
:*:;_a::ₐ
:*:;_e::ₑ
:*:;_h::ₕ
:*:;_i::ᵢ
:*:;_j::ⱼ
:*:;_k::ₖ
:*:;_l::ₗ
:*:;_m::ₘ
:*:;_n::ₙ
:*:;_o::ₒ
:*:;_p::ₚ
:*:;_r::ᵣ
:*:;_s::ₛ
:*:;_t::ₜ
:*:;_u::ᵤ
:*:;_v::ᵥ
:*:;_x::ₓ
:*:;__e::ₔ

::;asc0::
::;asc1::╔
::;asc2::╗
::;asc3::╚
::;asc4::╝
::;asc5::║
::;asc6::═
::;asc7::
::;asc8::
::;asc9::
::;asc10::

::;asc11::

::;asc12::

::;asc13::
::;asc14::
::;asc15::
::;asc16::
::;asc17::
::;asc18::
::;asc19::
::;asc20::
::;asc21::
::;asc22::
::;asc23::
::;asc24::
::;asc25::
::;asc26::
::;asc27::?
::;asc28::
::;asc29::
::;asc30::
::;asc31::
::;asc32::
::;asc33::!
::;asc34::"
::;asc35::#
::;asc36::$
::;asc37::%
::;asc38::&
::;asc39::'
::;asc40::(
::;asc41::)
::;asc42::*
::;asc43::+
::;asc44::,
::;asc45::-
::;asc46::.
::;asc47::/
::;asc48::0
::;asc49::1
::;asc50::2
::;asc51::3
::;asc52::4
::;asc53::5
::;asc54::6
::;asc55::7
::;asc56::8
::;asc57::9
::;asc58:::
::;asc59::;
::;asc60::<
::;asc61::=
::;asc62::>
::;asc63::?
::;asc64::@
::;asc65::A
::;asc66::B
::;asc67::C
::;asc68::D
::;asc69::E
::;asc70::F
::;asc71::G
::;asc72::H
::;asc73::I
::;asc74::J
::;asc75::K
::;asc76::L
::;asc77::M
::;asc78::N
::;asc79::O
::;asc80::P
::;asc81::Q
::;asc82::R
::;asc83::S
::;asc84::T
::;asc85::U
::;asc86::V
::;asc87::W
::;asc88::X
::;asc89::Y
::;asc90::Z
::;asc91::[
::;asc92::\
::;asc93::]
::;asc94::^
::;asc95::_
::;asc96::`
::;asc97::a
::;asc98::b
::;asc99::c
::;asc100::d
::;asc101::e
::;asc102::f
::;asc103::g
::;asc104::h
::;asc105::i
::;asc106::j
::;asc107::k
::;asc108::l
::;asc109::m
::;asc110::n
::;asc111::o
::;asc112::p
::;asc113::q
::;asc114::r
::;asc115::s
::;asc116::t
::;asc117::u
::;asc118::v
::;asc119::w
::;asc120::x
::;asc121::y
::;asc122::z
::;asc123::{
::;asc124::|
::;asc125::}
::;asc126::~